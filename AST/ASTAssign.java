package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TRef;
import values.IValue;
import values.VRef;

public class ASTAssign implements ASTNode {  //:=
	private ASTNode left;
	private ASTNode right;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to := operator";
	private static final String ERROR_TYPECHECK = "Wrong types in :=";
	
	public ASTAssign(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue ref = left.eval(env);
		IValue v = right.eval(env);
		if(ref instanceof VRef) {
			((VRef)ref).setRef(v);
			return ((VRef)ref).getRef();
		}
		throw new TypeError(ERROR_EVAL);	
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		// TODO Auto-generated method stub
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);


		if(l instanceof TRef) {
			if(r.equals(((TRef) l).getType())) {
				type = l;
				return type;
			}
		}		
		throw new TypeError(ERROR_TYPECHECK);
	}
	

}
