package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TBool;
import types.TInt;
import values.IValue;
import values.VBool;
import values.VInt;

public class ASTGreaterEqual implements ASTNode {
	private ASTNode left;
	private ASTNode right;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to >= operator";
	private static final String ERROR_TYPECHECK = "Wrong types in >=";
	
	public ASTGreaterEqual(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}


	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux1 = left.eval(env);
		IValue aux2 = right.eval(env);
		
		if(aux1 instanceof VInt) {
			if(aux2 instanceof VInt) {
				return new VBool(((VInt)aux1).getValue() >= ((VInt)aux2).getValue());	
			}
		}
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		String L1, L2;
		L1 = code.getLabel();
		L2 = code.getLabel();
		
		left.compile(env,code);
		right.compile(env,code);
		code.greater_equal(L1);
		code.num(0);
		code.jump(L2);
		code.anchor(L1);
		code.num(1);
		code.anchor(L2);		
	}
	
	
	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);

		if(l.equals(TInt.singleton) && r.equals(TInt.singleton)) {
			type = TBool.singleton;
			return type;
		}		
		throw new TypeError(ERROR_TYPECHECK);
	}

}
