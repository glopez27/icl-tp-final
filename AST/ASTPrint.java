package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import values.IValue;

public class ASTPrint implements ASTNode {
	private ASTNode node;
	private IType type;
	
	public ASTPrint(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux = node.eval(env);
		System.out.println(aux.show());
		
		return aux; 
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		code.print(node, env);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		type = node.typecheck(env);
		return type;
	}

}
