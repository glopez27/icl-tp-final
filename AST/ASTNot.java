package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TBool;
import values.IValue;
import values.VBool;

public class ASTNot implements ASTNode {
	private ASTNode node;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to ~ operator";
	private static final String ERROR_TYPECHECK = "Wrong types in ~";

	public ASTNot(ASTNode node) {
		this.node = node;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux1 = node.eval(env);
		if(aux1 instanceof VBool) {
			return new VBool(!((VBool)aux1).getBool());	
			
		}
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		String L1, L2;
		L1 = code.getLabel();
		L2 = code.getLabel();
		
		node.compile(env,code);
		code.not(L1);
		code.num(0);
		code.jump(L2);
		code.anchor(L1);
		code.num(1);
		code.anchor(L2);
	}
	
	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType aux = node.typecheck(env);

		if(aux.equals(TBool.singleton)) {
			type = aux;
			return type;
		}		
		throw new TypeError(ERROR_TYPECHECK);
	}

}
