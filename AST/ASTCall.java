package AST;

import java.util.List;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TClosure;
import values.IValue;
import values.VClosure;

public class ASTCall implements ASTNode{
	private ASTNode E1;
	private List<ASTNode> E2;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to call operator";
	private static final String ERROR_TYPECHECK = "Wrong types in call";
	
	public ASTCall(ASTNode E1, List<ASTNode> E2) {
		this.E1 = E1;
		this.E2 = E2;
	}
	

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux1 = E1.eval(env);
		
		if(aux1 instanceof VClosure) {
			IEnvironment<IValue> newEnv = ((VClosure) aux1).getEnv().beginScope();

			for(int i = 0; i<E2.size(); i++){
				IValue aux2 = E2.get(i).eval(env);
				newEnv.addAssoc(((VClosure) aux1).getId().get(i), aux2);
			}
			
			IValue res = ((VClosure) aux1).getBody().eval(newEnv);
			newEnv.endScope();
			
			return res;
		}
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		// TODO Auto-generated method stub
	}


	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType fun = E1.typecheck(env);
		
		if(fun instanceof TClosure) {
			if(((TClosure) E2).getTypesOfVariables().size() != E2.size()) {
				throw new TypeError(ERROR_TYPECHECK);
			}
			
			for(int i=0; i<E2.size(); i++) {
				IType aux1 = ((TClosure) fun).getTypesOfVariables().get(i);
				IType aux2 = E2.get(i).typecheck(env);
				
				if(!(aux1.equals(aux2))) {
					throw new TypeError(ERROR_TYPECHECK);
				}
			}
			type = ((TClosure) fun).getTypeOfBody();
			return type;
		}
		throw new TypeError(ERROR_TYPECHECK);
	}

	
	
}
