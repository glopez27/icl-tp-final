package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TInt;
import values.IValue;
import values.VInt;

public class ASTNum implements ASTNode {
	private int val;

	public ASTNum(int val) {
		this.val = val;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> env) {
		return new VInt(val);
	}
	
	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		code.num(val);
	}
	
	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		return TInt.singleton;
	}


}
