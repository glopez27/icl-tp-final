package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TRef;
import values.IValue;
import values.VRef;

public class ASTNew implements ASTNode {
	private ASTNode node;
	private IType type;
	
	public ASTNew(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux = node.eval(env);
		return new VRef(aux);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		// TODO Auto-generated method stub
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		type = new TRef(node.typecheck(env));
		return type;
	}

}
