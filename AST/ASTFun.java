package AST;

import java.util.List;
import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TClosure;
import values.IValue;
import values.VClosure;

public class ASTFun implements ASTNode { 
	private List<String> id; 
	private ASTNode node;
	private IType type;
	private List<IType> listOfTypes;
	private static final String ERROR_TYPECHECK = "Wrong types in fun";

	public ASTFun(List<String> id, ASTNode node, List<IType> listOfTypes) { 
		this.id = id;
		this.node = node;
		this.listOfTypes = listOfTypes;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		return new VClosure(id, env, node);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		// TODO Auto-generated method stub
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		
		if(listOfTypes.size() != id.size()) {
			throw new TypeError(ERROR_TYPECHECK);
		}
		
		IEnvironment<IType> newEnv = env.beginScope();
		
		for(int i = 0; i<listOfTypes.size(); i++) {
			newEnv.addAssoc(id.get(i), listOfTypes.get(i));
		}
		
		IType body = node.typecheck(newEnv);
		type = new TClosure(listOfTypes, body);
		return type;
	}
	
	public IType getType() {
		return type;
	}
	
}
