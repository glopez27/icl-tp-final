package AST;
import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import values.IValue;

public interface ASTNode {
	IValue eval(IEnvironment<IValue> env) throws TypeError;
	void compile(CompilerEnvironment env, CodeBlock code);
	IType typecheck(IEnvironment<IType> env) throws TypeError;
}
