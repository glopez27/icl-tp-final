package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import values.IValue;

public class ASTSeq implements ASTNode{  //;
	private ASTNode left;
	private ASTNode right;
	private IType type;
	
	public ASTSeq(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		left.eval(env);
		return right.eval(env);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		left.compile(env, code);
		code.pop();
		right.compile(env, code);			
	}


	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		type = right.typecheck(env);
		return type;
	} 

}
