package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TRef;
import values.IValue;
import values.VRef;

public class ASTDesref implements ASTNode {
	private ASTNode node;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to !(desref) operator";
	private static final String ERROR_TYPECHECK = "Wrong types in ! (desref)";
	
	public ASTDesref(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux = node.eval(env);
		
		if(aux instanceof VRef) {
			return ((VRef) aux).getRef();
		}	
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		// TODO Auto-generated method stub	
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType aux = node.typecheck(env);
		
		if(aux instanceof TRef) {
			type = ((TRef) aux).getType();
			return type;
		}
		throw new TypeError(ERROR_TYPECHECK);	
	}

}
