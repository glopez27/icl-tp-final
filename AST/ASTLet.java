package AST;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import values.IValue;

public class ASTLet implements ASTNode{
	private Map<String, ASTNode> mapAux;
	private ASTNode body;
	private IType type;
    private List<IType> idsTypes;
	private static final String ERROR_TYPECHECK = "Wrong types in let";

	
	public ASTLet(Map<String, ASTNode> map, ASTNode body, List<IType> idsTypes){  
		this.mapAux= map;
		this.body = body;
		this.idsTypes = idsTypes;
		
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IEnvironment<IValue> newEnv = env.beginScope();
		
		Set<Entry<String, ASTNode>> setOfMap = mapAux.entrySet();
		
		for(Entry<String, ASTNode> x: setOfMap) { 
			IValue v1 = x.getValue().eval(env); 
			newEnv.addAssoc(x.getKey(), v1);
		}
		
		IValue v2 = body.eval(newEnv);
		env= newEnv.endScope();
		return v2;
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		Set<Entry<String, ASTNode>> setOfMap = mapAux.entrySet();
		CompilerEnvironment newCompEnv = env.beginScope();
		code.createFrame(); 
		code.let(setOfMap, newCompEnv);
		body.compile(newCompEnv, code);
		code.deleteFrame();
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		Set<Entry<String, ASTNode>> setOfMap = mapAux.entrySet();
		
		int counter = 0;
		
		for(Entry<String, ASTNode> x: setOfMap) { 
			IType aux1 = x.getValue().typecheck(env);
			IType aux2 = idsTypes.get(counter);
			
			if(!(aux1.equals(aux2))) {
				throw new TypeError(ERROR_TYPECHECK);
			}
			counter++;
		}
		
		IEnvironment<IType> newEnv = env.beginScope(); 

		counter = 0;
		for(Entry<String, ASTNode> x: setOfMap) {
			newEnv.addAssoc(x.getKey(), idsTypes.get(counter));
			counter++;
		}

	    type = body.typecheck(newEnv); 
		env = newEnv.endScope();
	     
	    return type;   
	}


}
