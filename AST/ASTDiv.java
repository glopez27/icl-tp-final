package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TInt;
import values.*;

public class ASTDiv implements ASTNode {
	private ASTNode lN;
	private ASTNode rN;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to / operator";
	private static final String ERROR_TYPECHECK = "Wrong types in /";

	public ASTDiv(ASTNode n1, ASTNode n2) {
		lN = n1;
		rN = n2;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux1 = lN.eval(env);
		IValue aux2 = rN.eval(env);
		
		if(aux1 instanceof VInt) {
			if(aux2 instanceof VInt) {
				return new VInt(((VInt)aux1).getValue()/((VInt)aux2).getValue());	
			}
		}
		throw new TypeError(ERROR_EVAL); 	
	}
	
	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		lN.compile(env, code);
		rN.compile(env, code);
		code.div();
	}
	
	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType l = lN.typecheck(env);
		IType r = rN.typecheck(env);

		if(l.equals(TInt.singleton) && r.equals(TInt.singleton)) {
			type = l;
			return type;
		}
			throw new TypeError(ERROR_TYPECHECK);
	}


}
