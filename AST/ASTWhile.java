package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TBool;
import values.IValue;
import values.VBool;

public class ASTWhile implements ASTNode {
	private ASTNode E1;
	private ASTNode E2;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to while operator";
	private static final String ERROR_TYPECHECK = "Wrong types in while";
	
	public ASTWhile(ASTNode E1, ASTNode E2) {
		this.E1 = E1;
		this.E2 = E2;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux_cond = E1.eval(env);
		
		if(aux_cond instanceof VBool) {
			IValue aux_do = null;
			while(((VBool) aux_cond).getBool() == true) {
				aux_do = E2.eval(env); 
				aux_cond = E1.eval(env);
			}
			VBool res = new VBool(false);
			return res;
		}
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		String L1, L2;
		L1 = code.getLabel(); 
		L2 = code.getLabel(); 
		
		code.anchor(L1);
		E1.compile(env, code); 
		
		code.equals(L2); 
		
		E2.compile(env, code);
		code.jump(L1);
		code.anchor(L2);	
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType cond = E1.typecheck(env);
		
		if(cond.equals(TBool.singleton)) {
			type = cond;
			return type;	
		}
		throw new TypeError(ERROR_TYPECHECK);
	}

}
