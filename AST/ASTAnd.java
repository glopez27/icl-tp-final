package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TBool;
import values.IValue;
import values.VBool;

public class ASTAnd implements ASTNode {
	private ASTNode left;
	private ASTNode right;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to && operator";
	private static final String ERROR_TYPECHECK = "Wrong types in &&";

	
	public ASTAnd(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux1 = left.eval(env);
		IValue aux2 = right.eval(env);
		
		if(aux1 instanceof VBool) {
			if(aux2 instanceof VBool) {
				return new VBool(((VBool)aux1).getBool()&&((VBool)aux2).getBool());	
			}
		}
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		left.compile(env, code);
		right.compile(env, code);
		code.and();
	}
	
	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);

		if(l.equals(TBool.singleton) && r.equals(TBool.singleton)) {
			type = l;
			return type;	
		}
		throw new TypeError(ERROR_TYPECHECK);
	}


}
