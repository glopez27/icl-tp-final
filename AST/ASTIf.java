package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TBool;
import values.IValue;
import values.VBool;

public class ASTIf implements ASTNode {
	private ASTNode condition;
	private ASTNode res_true;
	private ASTNode res_false;
	private IType type;
	private static final String ERROR_EVAL = "Illegal arguments to if operator";
	private static final String ERROR_TYPECHECK = "Wrong types in if";
	

	public ASTIf(ASTNode condition, ASTNode res_true, ASTNode res_false) {
		this.condition = condition;
		this.res_true = res_true;
		this.res_false = res_false;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		IValue aux_cond = condition.eval(env);
		
		if(aux_cond instanceof VBool) {
			if(((VBool)aux_cond).getBool() == true) {
				IValue aux_true = res_true.eval(env);
				return aux_true;
			}
			else {
				IValue aux_false = res_false.eval(env);
				return aux_false;
			}
		}
		throw new TypeError(ERROR_EVAL);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		String L1, L2;
		L1 = code.getLabel(); //false
		L2 = code.getLabel(); //true
		
		//compare 0 with the bool value of condition
		condition.compile(env, code); 
		code.num(0);
		code.equals(L1); 
		
		//if it's true
		res_true.compile(env, code);
		code.jump(L2);
			
		//if it's false
		code.anchor(L1);
		res_false.compile(env, code);
		code.anchor(L2);
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		IType cond = condition.typecheck(env);

		if(cond.equals(TBool.singleton)) {
			IType c_then = res_true.typecheck(env);
			IType c_else = res_false.typecheck(env);
			
			if(c_then.equals(c_else)) {
				type = c_else;
				return type;
			}
		}
		
		throw new TypeError(ERROR_TYPECHECK);
	}

}
