package AST;

import Env.IEnvironment;
import compiler.Address;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import values.IValue;

public class ASTId implements ASTNode{
	private String id;
	private IType type;
	
	
	public ASTId(String id){
		this.id = id;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) {
		return env.find(id);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		Address a = env.find(id, 0);
		code.id(a);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		type = env.find(id);
		return type;
	}

}
