package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;
import exceptions.TypeError;
import types.IType;
import types.TBool;
import values.IValue;
import values.VBool;

public class ASTBool implements ASTNode {
	private boolean val;
	
	public ASTBool(boolean val) {
		this.val = val;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws TypeError {
		return new VBool(val);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		code.bool(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypeError {
		return TBool.singleton;
	}

}
