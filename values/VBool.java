package values;

public class VBool implements IValue{
	private boolean value;
	
	public VBool(boolean value) {
		this.value = value;
	}
	
	public boolean getBool() {
		return value;
	}

	@Override
	public String show() {
		return "" + value;
	}

}
