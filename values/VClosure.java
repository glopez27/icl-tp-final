package values;

import java.util.List;

import AST.ASTNode;
import Env.IEnvironment;

public class VClosure implements IValue {
	private List<String> id;
	private IEnvironment<IValue> env;
	private ASTNode body;

	public VClosure(List<String> id, IEnvironment<IValue> env, ASTNode body) {
		this.id = id;
		this.env = env;
		this.body = body;
	}
	
	public List<String> getId() {
		return id;
	}
	
	public IEnvironment<IValue> getEnv() {
		return env;
	}
	
	public ASTNode getBody() {
		return body;
	}
	
	
	@Override
	public String show() {
		// TODO Auto-generated method stub
		return null;
	}

}
