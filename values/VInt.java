package values;

public class VInt implements IValue{
	private int value;
	
	public VInt(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

	@Override
	public String show() {
		return ""+value;
	}
	
	
}
