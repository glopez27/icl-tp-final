package values;

public class VRef implements IValue{
	private IValue value;
	
	public VRef(IValue value) {
		this.value = value;
	}
	
	public IValue getRef() {
		return value;
	}
	
	public void setRef(IValue value) {
		this.value = value;
	}

	@Override
	public String show() {
		return value.show();
	}
	

}
