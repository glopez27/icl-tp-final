package compiler;
import java.io.PrintStream;

public class StackFrame {
	public final static String VARIABLE = "loc_";
	public final static String SL_CONST = "SL"; 
	
	private int id; 
	private StackFrame SL; 
	private int nr_var; 


	public StackFrame(StackFrame SL,int id) {
		this.id = id;
		this.SL = SL;
		nr_var = 0;
	}
	
	public int getId() {
		return id;
	}
	
	public StackFrame getSL() {
		return SL;
	}
	

	public void addVariable(){ 
		nr_var++;
	}
	
	public int getNrVar() {
		return nr_var;
	}

	
	public void dump(PrintStream out) {
		out.println(".class frame" + id);
    	out.println(".super java/lang/Object");
    	if(SL != null)
    		out.println(".field public sl Lframe" + SL.getId() + ";");
    	else
    		out.println(".field public sl Ljava/lang/Object;");
  		
    	for(int i=0; i < nr_var; i++)
    		out.println(".field public x" + i + " I");
    	out.println("\n.method public <init>()V");
    	out.println("	aload_0");
    	out.println("	invokenonvirtual java/lang/Object/<init>()V");
    	out.println("	return");
    	out.println(".end method");
    } 
	


}
