package compiler;
public class Address {
	private int level;
	private int offset;
	
	public Address(int level, int offset) {
		this.level = level;
		this.offset = offset;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getOffset() { 
		return offset;
	}

}
