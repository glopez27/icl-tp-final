package compiler;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map.Entry;

import AST.ASTNode;


public class CodeBlock {
	
	private ArrayList<String> code; //msg
	private ArrayList<StackFrame> frames; //todas as frames necessarias (nr de let's)
	private StackFrame current; 
	private int counter;

	
	public CodeBlock() {
		current = null;
		code = new ArrayList<String>();
		frames = new ArrayList<StackFrame>();
		counter = 0;
	}
	
	public String getLabel() {
		return "L"+(counter++);
	}
	
	public void div() {
		code.add("idiv");
	}
	
	public void num(int n) {
		code.add("sipush " + n);
	}

	public void add() {
		code.add("iadd");
	}
	
	public void sub() {
		code.add("isub");
	}
	
	public void mul() {
		code.add("imul");
	}
	
	public void dup() {
		code.add("dup");
	}
	
	public void and() {
		code.add("iand");
	}
	
	public void or() {
		code.add("ior");
	}
	
	public void bool(boolean val) {
		if(val)
			code.add("sipush 1");
		else
			code.add("sipush 0"); 
	}
	
	public void not(String label) {
		code.add("if_icmpne " + label);
	}
	
	public void lesser(String label) {
		code.add("if_icmplt " + label);
	}
	
	public void lesser_equal(String label) {
		code.add("if_icmple " + label);
	}
	
	public void greater(String label) {
		code.add("if_icmpgt " + label);
	}

	public void greater_equal(String label) {
		code.add("if_icmpge " + label);
	}
	
	public void equals(String label) {
		code.add("if_icmpeq " + label);
	}
	
	//Jump to label.
	public void jump(String label) {
		code.add("goto " + label);
	}
	
	public void anchor(String label) {
		code.add(label + ":");
	}	
	
	public void pop() {
		code.add("pop");
	}
	
	
	public void id(Address a) { 
		StackFrame temp = current;
		
		code.add("aload 4");
		for(int i = 0; i < a.getLevel(); i++){
			code.add("getfield frame"  + temp.getId() + "/sl Lframe" + temp.getSL().getId() + ";"); 
			temp = temp.getSL();
		}
		code.add("getfield frame"  + temp.getId() + "/x" + a.getOffset() + " I"); 
	}
	
	public void print(ASTNode node, CompilerEnvironment env) {
		code.add("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		node.compile(env, this);
		code.add("       ; convert to String;");
		code.add("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		code.add("       ; call println ");
		code.add("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		node.compile(env, this);		
	}
	
	public void let(Set<Entry<String, ASTNode>> set, CompilerEnvironment env) {
		for(Entry<String, ASTNode> e: set) { 
			code.add("aload 4"); 
			e.getValue().compile(env, this); 
			env.addAssoc(e.getKey(), current.getNrVar()); 
			code.add("putfield frame"  + current.getId() + "/x" + current.getNrVar() + " I");
			current.addVariable();
		}
	}
	
	public void createFrame() {
		StackFrame frame = new StackFrame(current, frames.size()); 
		code.add("new frame" + frame.getId() );
		code.add("dup");
		code.add("invokespecial frame" + frame.getId() + "/<init>()V");
		code.add("dup");
		code.add("aload 4"); 
		if(current == null)
			code.add("putfield frame"  + frame.getId() + "/sl Ljava/lang/Object;");
		else
			code.add("putfield frame"  + frame.getId() + "/sl Lframe" + current.getId() + ";"); 
		code.add("astore 4");
		current = frame;
		frames.add(frame);
	}
	
	public void deleteFrame() {
		code.add("aload 4"); 
		if(current.getId() == 0)
			code.add("getfield frame"  + current.getId() + "/sl Ljava/lang/Object;");
		else
			code.add("getfield frame"  + current.getId() + "/sl Lframe" + current.getSL().getId() + ";"); 	
		code.add("astore 4");
		current = current.getSL();
	}
		
	
	//escrever codigo principal
	public String dumpCode() {
		
		String res="";
		for(String s : code) {
			res+= ("       "+s+"\n");
		}
		return res;
	}
	
	//escrever os pequenos codigos das frames
	void dumpFrames() throws FileNotFoundException { 
		for( StackFrame frame: frames) {
			PrintStream out = new PrintStream(new FileOutputStream("frame" + frame.getId()+".j"));
			frame.dump(out);
		}
	} 
	

}
