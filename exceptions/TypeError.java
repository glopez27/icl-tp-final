package exceptions;

public class TypeError extends Exception {

	private static final long serialVersionUID = 1L;

	public TypeError() {
	}

	public TypeError(String message) {
		super(message);
	}
}