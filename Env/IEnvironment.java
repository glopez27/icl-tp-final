package Env;

public interface IEnvironment<T> {
	IEnvironment<T> beginScope();
	IEnvironment<T> endScope();
	void addAssoc(String id, T value);
	T find(String id);

}
