package Env;

import java.util.HashMap;
import java.util.Map;

public class Environment<T> implements IEnvironment<T>{
	private Environment<T> parent;
	private Map<String, T> map;
	
	public Environment(Environment<T> parent){
		this.parent = parent;
		this.map = new HashMap<String, T>();
	}

	@Override
	public IEnvironment<T> beginScope() {
		Environment<T> env = new Environment<T>(this);
		return env;
	}

	@Override
	public IEnvironment<T> endScope() {
		return parent;
	}

	@Override
	public void addAssoc(String id, T value) {
		map.put(id, value);
	}

	@Override
	public T find(String id) {
		if(map.containsKey(id)) {
			return map.get(id);
		}
		return parent.find(id);
	}

}
