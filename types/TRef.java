package types;

public class TRef implements IType{
	private IType type;
	
	public TRef(IType type) {
		this.type = type;
	}
	
	@Override
	public boolean equals(Object n) {
		boolean res = false;
		
		if(n instanceof TRef) {
			res = ((TRef) n).getType().equals(type);
		}	
		return res;
	}
	
	public IType getType() {
		return type;
	}

}
