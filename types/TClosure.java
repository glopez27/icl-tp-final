package types;

import java.util.List;

public class TClosure implements IType{
	
	private List<IType> types; //tipos das variaveis
	private IType body;

	public TClosure(List<IType> types, IType body) {
		this.types = types;
		this.body = body;
	}
	
	@Override
	public boolean equals(Object n) {
		boolean res = false;
		
		if(n instanceof TClosure) {
			
			if(types.size() != ((TClosure) n).getTypesOfVariables().size()) {
				return false;
			}
			
			for(int i = 0; i<types.size(); i++) {
				if(!(types.get(i).equals(((TClosure) n).getTypesOfVariables().get(i)))) {
					return false;
				}
			}
			
			if(!(body.equals(((TClosure) n).getTypeOfBody()))) {
				return false;
			}
			res = true;	
		}	
		return res;
	}

	
	public IType getTypeOfBody() {
		return body;
	}
	
	public List<IType> getTypesOfVariables(){
		return types;
	}
}
